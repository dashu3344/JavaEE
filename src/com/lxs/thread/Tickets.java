package com.lxs.thread;

public class Tickets implements Runnable {
    private int tickets = 100;

    private Object object = new Object();

    @Override
    public void run() {

        while (true) {
            synchronized (object) {
                if (tickets > 0) {
                    System.out.println
                            (Thread.currentThread().getName() + "出售" + tickets--);
                }
            }
        }
    }
}
