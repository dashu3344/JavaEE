package com.lxs.thread;

public class Tickets2 implements Runnable {
    private int tickets = 100;

    @Override
    public void run() {

        while (true) {
            sellTickets();
        }
    }

    public synchronized void sellTickets() {
        if (tickets > 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName()
                    + "售票===" + tickets--);

        }
    }
}
