package com.lxs.thread;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadTest {
    public static void main(String[] args) {
//        System.out.println(0 / 0);
//        ThreadDemo1 demo1 = new ThreadDemo1();
//
//        ThreadDemo2 demo2 = new ThreadDemo2();
//
//        demo1.start();
//
//        demo2.start();
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        }).start();

//        threadpool();
        Tickets tickets = new Tickets();

        Tickets2 tickets2 = new Tickets2();
        Thread thread1 = new Thread(tickets2);

        Thread thread2 = new Thread(tickets2);

        Thread thread3 = new Thread(tickets2);

        thread1.start();
        thread2.start();
        thread3.start();

    }

    private static void threadpool() {
        ExecutorService threadPool = Executors.newFixedThreadPool(2);

        threadPool.submit(new Thread3());
        threadPool.submit(new Thread3());
    }
}

class ThreadDemo1 extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 50; i++) {
            System.out.println("Demo1==============" + i);
        }
    }
}

class ThreadDemo2 extends Thread {
    public ThreadDemo2(Runnable runnable) {

    }

    @Override
    public void run() {
        super.run();

        for (int i = 0; i < 50; i++) {
            System.out.println("Demo2==============" + i);
        }
    }
}

class Thread3 implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 50; i++) {
            System.out.println("Thread3====" + i);
        }
    }
}