package com.lxs.io;

import java.io.*;

public class FileDemo {

    private static FileOutputStream outputStream;


    public static void fileTest() {
//        是一个分号,目录的分割(window中环境变量配置各个路径用分号分割，表示一个完整的路径结束)  Linux中是冒号 :
        String separator = File.pathSeparator;
        System.out.println(separator);
        //与系统有关的默认名称分隔符
        separator = File.separator;
        System.out.println(separator);// 向右 \  目录名称分割  Linux /
    }

    public static void newFile(String name) {
//        File file = new File(name);
//        System.out.println(file);

        File parent = new File("D:");
        File file = new File(parent, "\\TestFile2");

//        System.out.println(file);
        File file2 = new File("D:", "Test");
//        System.out.println(file2);

        File file1 = new File("D:\\file.txt");
        try {
            boolean newFile = file1.createNewFile();

            boolean mkdirs = file1.mkdirs();

//            	  public boolean mkdir():创建文件夹 如果存在这样的文件夹，就不创建了
//                public boolean mkdirs():创建文件夹,如果父文件夹不存在，会帮你创建出来
//                       String getName(): 返回路径中表示的文件或者文件夹名     获取路径中的最后部分的名字
//                    long length(): 返回路径中表示的文件的字节数
//                    String getAbsolutePath(): 获取绝对路径,返回String对象
//                    File   getAbsoluteFile() : 获取绝对路径,返回File对象
//                    eclipse环境中,写一个相对路径,绝对位置工程根目录
//                    String getParent(): 获取父路径,返回String对象
//                    File getParentFile(): 获取父路径,返回File对象


//            * boolean exists(): 判断File构造方法中封装路径是否存在
//                    * 存在返回true,不存在返回false
//                    * boolean isDirectory(): 判断File构造方法中封装的路径是不是文件夹
//                    * 如果是文件夹,返回true,不是文件返回false
//                    * boolean isFile(): 判断File构造方法中封装的路径是不是文件
//                    * 如果是文件,返回true,不是文件返回false

            String[] list = file1.list();
            File[] files = file1.listFiles();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void writeFile() {
        File testFile = new File("D:", "TestFile.txt");

        if (!testFile.exists()) {
            try {
                boolean newFile = testFile.createNewFile();
                System.out.println(newFile + "=======================");

                outputStream = new FileOutputStream(testFile);

                byte[] string1 = {121, 32, 42, 41};
                outputStream.write(string1, 1, 3);
                outputStream.write("Hello".getBytes());
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                FileOutputStream outputStream = new FileOutputStream(testFile, true);

                outputStream.write("Test\r\neeeeeeeeeeeeeeeeeee".getBytes());

                outputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

        /*	* abstract  int read() ：
            * 从输入流中读取数据的下一个字节。
		    * int read(byte[] b)
			* 从输入流中读取一定数量的字节，并将其存储在缓冲区数组 b 中。
		    * int read(byte[] b, int off, int len)
			* 将输入流中最多 len 个数据字节读入 byte 数组。
		    * void close()
		    *
		    *
		    *
			* 关闭此输入流并释放与该流关联的所有系统资源。
            *   字节输入流
		    *     java.io.InputStream 所有字节输入流的超类
            *   作用: 读取任意文件,每次只读取1个字节
            *   读取的方法  read
            *     int  read() 读取1个字节
            *     int  read(byte[] b) 读取一定量的字节,存储到数组中
            */

    public static void readFile() {
        try {
            FileInputStream inputStream = new FileInputStream("D:" + File.separator + "TestFile.txt");

            int len = 0;
            while ((len = inputStream.read()) != -1) {

                System.out.println((char) len);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
        }
    }

    public static void readFile1() {
        try {
            FileInputStream inputStream = new FileInputStream("D:" + File.separator + "TestFile.txt");

            byte[] bytes = new byte[1024];

            int len = 0;
            while ((len = inputStream.read(bytes)) != -1) {
                System.out.println(new String(bytes, 0, len));
            }

            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copy() {
        FileOutputStream outputStream = null;
        FileInputStream inputStream = null;
        File fileA = new File("C:\\Users\\15658\\Desktop", "第一行代码 Android 第2版.pdf");
        File fileB = new File("D:", "第一行代码 Android 第2版.pdf");

        try {
            if (!fileB.exists()) {
                boolean newFile = fileB.createNewFile();
            }
            if (fileA.exists()) {
                inputStream = new FileInputStream(fileA);
                outputStream = new FileOutputStream("D:\\第一行代码 Android 第2版.pdf");
                int len = 0;
                byte[] bytes = new byte[1024 * 10];

                while ((len = inputStream.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, len);
                }
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {

        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void outWrite() throws FileNotFoundException, UnsupportedEncodingException {
        FileOutputStream fos = new FileOutputStream("D:\\TestFile.txt");

        OutputStreamWriter writer = new OutputStreamWriter(fos, "UTF-8");

        try {
            writer.write("\r\n好");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void inPutReader() {
        try {
            FileInputStream fis = new FileInputStream("D:\\TestFile.txt");

            try {
                InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
                char[] bytes = new char[1024];
                try {
                    int len = isr.read(bytes);

                    System.out.println(new String(bytes, 0, len));

                    isr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
